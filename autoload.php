<?php
session_start();

define("URL_ROOT", "./");
define("DOC_ROOT", $_SERVER["DOCUMENT_ROOT"]);
define("PEGE_PATH", DOC_ROOT . "pages/");
define("CONFIG_PATH", DOC_ROOT . "config/");
define("MEDIA_PATH", URL_ROOT . "assets/");
define("COMPONENTS_PATH", DOC_ROOT . "components/");

include CONFIG_PATH . 'db.php';
include CONFIG_PATH . 'function.php';

$page = @$_GET["page"];

$page = empty($page) ? "home" : $page;

$file = $page . ".php";

$render_page = file_exists(PEGE_PATH . $file) ? PEGE_PATH . $file : PEGE_PATH . "404.php";


