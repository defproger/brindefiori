<?php

$files = scandir(MEDIA_PATH . "css/");

$array = array_diff($files, [".", "..","cancel.css"]);

echo "<link rel='stylesheet' href='./assets/css/cancel.css'>";

foreach ($array as $file) {
    if ($file != "css.php") {
        $filepath = MEDIA_PATH . "css/" . $file;
        echo "<link rel='stylesheet' href='{$filepath}'>";
    }
}
?>
