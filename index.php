<?php include 'autoload.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>

    <?php include MEDIA_PATH . 'css/css.php'; ?>
    <?php include MEDIA_PATH . 'js/js.php'; ?>

</head>
<body>
<?php include COMPONENTS_PATH . 'back.php'; ?>

<div class="mainblock">
    <?php //include COMPONENTS_PATH . 'header.php'; ?>
    <main><?php include $render_page; ?></main>
</div>

</body>
</html>
