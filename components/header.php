<?php
$links = [
    [
        'path' => '/',
        'name' => 'home'
    ],
    [
        'path' => '/?page=about',
        'name' => 'about'
    ],
    [
        'path' => '/?page=contact',
        'name' => 'contact'
    ]
];


?>
<header>
    <ul>
        <?php foreach ($links as $link): ?>
            <li>
                <a class="<?= $link['name'] == $page ? 'active' : ''; ?>"
                   href="<?= $link['path']; ?>"><?= $link['name']; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</header>