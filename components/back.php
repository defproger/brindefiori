<?php ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Глобальные стили</title>
    <style>
        .button {
            transform: skewX(-45deg);
        }

        .size {
            border: 0px ridge #f00;
            background-color: #ff0;
            position: fixed;
            width: 60%;
            height: 100%;
        }

        .first {
            top: 0;
            left: -25%;
            background-color: #ff0;
            z-index: 6;
        }

        .second {
            top: 0;
            left: 25%;
            background-color: #ff0000;
            z-index: 4;
        }

        .third {
            top: 0;
            left: 75%;
            background-color: cyan;
            z-index: 2;
        }

        .box {
            animation-duration: 3s;
            animation-iteration-count: 0.5;
        }

        .bounce-1, .bounce-2,.bounce-3 {
            animation-timing-function: linear;
        }

        .bounce-1{
            animation-name: bounce-1;
        }

        .bounce-2{
            animation-name: bounce-2;
        }

        .bounce-3{
            animation-name: bounce-3;
        }

        @keyframes bounce-1 {
            50% {
                transform: translateX(-140%) skewX(-45deg);
            }
            100% {
                transform: translateX(-140) skewX(-45deg);
            }
        }

        @keyframes bounce-2 {
            50% {
                transform: translateX(-240%) skewX(-45deg);
            }
            100% {
                transform: translateX(-240) skewX(-45deg);
            }
        }

        @keyframes bounce-3 {
            50% {
                transform: translateX(-340%) skewX(-45deg);
            }
            100% {
                transform: translateX(-340) skewX(-45deg);
            }
        }

    </style>
</head>
<body onscroll="myFunction()">
<div class="wid">
    <div class="size first col1 button box bounce-1">
    </div>
    <div class="size second col1 button box bounce-2">
    </div>
    <div class="size third col1 button box bounce-3">
    </div>
</div>
</body>

</html>

